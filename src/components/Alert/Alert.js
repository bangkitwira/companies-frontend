import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

export default function Alert(props) {
    const { isOpen, message, onClose } = props
    const [open, setOpen] = React.useState();

    useEffect(() => {
        setOpen(isOpen)
        setTimeout(() => {
            setOpen(false)
            onClose(open)
        }, 4000);
        return () => {
            setOpen()
        }
    }, [isOpen])

    const handleClose = (event, reason) => {
        setOpen(false);
    };

    return (
        <div>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
                message={message}
                color="danger"
                action={
                    <React.Fragment>
                        {/* <Button color="secondary" size="small" onClick={handleClose}>
                            UNDO
                        </Button> */}
                        <IconButton size="small" aria-label="close" color="inherit" onClick={() => onClose(open)}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        </div>
    );
}
import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Business from '@material-ui/icons/Business';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { DateTime } from 'luxon'
import { addCompany } from '../../services/Company.service';
import Alert from '../Alert/Alert';
import { UseAuth } from '../../hooks/UseAuth';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    formControl: {
        width: '100%'
    }
}));

const Register = () => {
    const classes = useStyles();
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const [namaPerusahaan, setNamaPerusahaan] = useState()
    const [tanggalBerdiri, setTanggalBerdiri] = useState(DateTime.now().toISODate())
    const [tipePerusahaan, setTipePerusahaan] = useState("")
    const [isError, setIsError] = useState(false)
    const [message, setMessage] = useState()
    const { authData } = UseAuth()

    useEffect(() => {

        return () => {
            setTipePerusahaan("")
            setIsError(false)
        }
    }, [])


    const handleChange = (event) => {
        setTipePerusahaan(event.target.value);
    };
    const submitData = async (e) => {
        e.preventDefault();
        const token = authData.token
        const data = await addCompany({ firstName, lastName, namaPerusahaan, tanggalBerdiri, tipePerusahaan }, token)
        if (data) {
            if (data.message) {
                setIsError(true)
                setMessage(data.message)
            }
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <Alert isOpen={isError} message={message} onClose={(e) => setIsError(e)} />
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <Business />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up your company
                </Typography>
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="firstName"
                                variant="outlined"
                                required
                                fullWidth
                                id="firstName"
                                label="First Name"
                                autoFocus
                                onChange={(e) => setFirstName(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="lastName"
                                label="Last Name"
                                name="lastName"
                                autoComplete="lname"
                                onChange={(e) => setLastName(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="company"
                                label="Nama Perusahaan"
                                name="company"
                                autoComplete="company"
                                onChange={(e) => setNamaPerusahaan(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="date"
                                label="Tanggal Berdiri"
                                type="date"
                                variant="outlined"
                                fullWidth
                                defaultValue={DateTime.now().toISODate()}
                                onChange={(e) => setTanggalBerdiri(e.target.value)}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel id="demo-simple-select-outlined-label">Tipe Perusahaan</InputLabel>
                                <Select
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    value={tipePerusahaan}
                                    onChange={handleChange}
                                    label="Tipe Perusahaan"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value={"perseorangan"}>Perseorangan</MenuItem>
                                    <MenuItem value={"cv"}>CV</MenuItem>
                                    <MenuItem value={"firma"}>Firma</MenuItem>
                                    <MenuItem value={"pt"}>PT</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={submitData}
                    >
                        Daftar
                    </Button>
                </form>
            </div>
        </Container>
    );
}

export default Register;
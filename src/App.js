import './App.css';
import { BrowserRouter as Router } from "react-router-dom";

import Main from './Layout/Main';
import { AuthProvider } from './hooks/UseAuth';



function App() {
  
  const user = JSON.parse(localStorage.getItem("user"))
  
  return (
    <div className="App">
      <AuthProvider user={user}>
        <Router>
          <Main />
        </Router>
      </AuthProvider>
    </div>
  );
}

export default App;

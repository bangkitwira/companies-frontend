export function auth(userData) {
    return fetch('http://127.0.0.1:4000/users/authenticate',
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(userData)
        }
    ).then(resp => resp.json()).catch(e => {
        console.log(e)
    })
}
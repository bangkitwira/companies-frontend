export function addCompany(companyData, token) {
    return fetch('http://127.0.0.1:4000/company/add',
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify(companyData)
        }
    ).then(resp => resp.json()).catch(e => {
        console.log(e)
    })
}
import React from 'react'
import { Switch, Route } from "react-router-dom";

import Login from '../components/Login/Login';
import Register from '../components/RegisterCompanies/Register';
import Sidebar from './Sidebar';
import { UseAuth } from '../hooks/UseAuth';
import { useHistory } from "react-router-dom";


const Main = () => {
    const { authData } = UseAuth()
    const history = useHistory()
    if (!authData) {
        history.replace('/login')
    }

    return (
        <div className="main">
            {authData && authData.username ? (
                <>
                    <Sidebar />
                    <Switch>
                        <Route path="/">
                            <Register />
                        </Route>
                    </Switch>
                </>
            ) :
                (
                    <>
                        <Switch>
                            <Route exact path="/login">
                                <Login />
                            </Route>
                        </Switch>
                    </>
                )
            }
        </div>
    )
}

export default Main
